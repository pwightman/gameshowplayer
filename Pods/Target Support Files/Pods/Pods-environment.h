
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// DTBonjour
#define COCOAPODS_POD_AVAILABLE_DTBonjour
#define COCOAPODS_VERSION_MAJOR_DTBonjour 1
#define COCOAPODS_VERSION_MINOR_DTBonjour 0
#define COCOAPODS_VERSION_PATCH_DTBonjour 0

// ObjectAL-for-iPhone
#define COCOAPODS_POD_AVAILABLE_ObjectAL_for_iPhone
#define COCOAPODS_VERSION_MAJOR_ObjectAL_for_iPhone 2
#define COCOAPODS_VERSION_MINOR_ObjectAL_for_iPhone 5
#define COCOAPODS_VERSION_PATCH_ObjectAL_for_iPhone 0

// PSAlertView
#define COCOAPODS_POD_AVAILABLE_PSAlertView
#define COCOAPODS_VERSION_MAJOR_PSAlertView 1
#define COCOAPODS_VERSION_MINOR_PSAlertView 1
#define COCOAPODS_VERSION_PATCH_PSAlertView 0

