//
//  GSInGameViewController.m
//  GameShowPlayer
//
//  Created by Parker Wightman on 3/12/13.
//  Copyright (c) 2013 Alora Studios. All rights reserved.
//

#import "GSInGameViewController.h"
#import <ObjectAL-for-iPhone/ObjectAL/ObjectAL.h>

@interface GSInGameViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation GSInGameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _player.delegate = self;
    [[OALSimpleAudio sharedInstance] preloadEffect:@"buzzedEarly.wav"];
    [[OALSimpleAudio sharedInstance] preloadEffect:@"buzzedIn.wav"];
	// Do any additional setup after loading the view.
}

- (IBAction)buzzTapped:(id)sender {
    [_player sendBuzzIn];
}

- (void) viewDidAppear:(BOOL)animated {
}

- (void) player:(GSPlayer *)player didReceivePlayersUpdate:(NSArray *)players {
    NSLog(@"Received score update: %@", players);
    _players = players;
    [_tableView reloadData];
}

- (void) playerDidWinBuzz:(GSPlayer *)player {
    [[OALSimpleAudio sharedInstance] playEffect:@"buzzedIn.wav"];
}

- (void) playerDidBuzzEarly:(GSPlayer *)player {
    [[OALSimpleAudio sharedInstance] playEffect:@"buzzedEarly.wav"];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _players.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"playerCell"];
    
    cell.textLabel.text = [[_players objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [[_players objectAtIndex:indexPath.row] objectForKey:@"score"]];
    
    return cell;
}

@end
