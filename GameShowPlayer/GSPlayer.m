//
//  GSPlayer.m
//  GameShowPlayer
//
//  Created by Parker Wightman on 3/12/13.
//  Copyright (c) 2013 Alora Studios. All rights reserved.
//

#import "GSPlayer.h"
#import "GSProtocol.h"

@interface GSPlayer () <DTBonjourDataConnectionDelegate>

@end

@implementation GSPlayer

+ (id) playerWithDataConnection:(DTBonjourDataConnection *)dataConnection {
    GSPlayer *player = [[GSPlayer alloc] init];
    
    player.dataConnection = dataConnection;
    player.dataConnection.delegate = player;
    player.dataConnection.sendingContentType = DTBonjourDataConnectionContentTypeJSON;
    
    return player;
}

- (void) sendBuzzIn {
    [_dataConnection sendObject:@{ @"protocol": @(GSProtocolBuzzIn) } error:nil];
}

- (void) sendSignIn {
    [self openConnectionIfNeeded];
    
    NSLog(@"Attempting sign in...");
    
    NSError *error = nil;
    if ( ![_dataConnection sendObject:@{ @"protocol": @(GSProtocolSignInRequest) } error:&error] ) {
        NSLog(@"Can't send object!");
    }
    
    if ( error ) {
        NSLog(@"Error: attempt to send sign in request failed.");
    }
    
}

- (void) sendTeamName {
    NSError *error = nil;
    [_dataConnection sendObject:@{ @"protocol": @(GSProtocolTeamNameRequest), @"name": _name } error:&error];
}

- (void) openConnectionIfNeeded {
//    if ( _dataConnection.isOpen ) {
        NSLog(@"Got here.");
        if ( ![_dataConnection open] ) {
            NSLog(@"Error: couldn't open connection to net service");
        }
//    }
}

- (void) connection:(DTBonjourDataConnection *)connection didReceiveObject:(id)object {
    GSProtocol protocol = [[object objectForKey:@"protocol"] integerValue];
    
    NSLog(@"Recived protocol: %@", @(protocol));
    
    if ( protocol == GSProtocolTeamNameRequest ) {
        [_delegate playerDidReceiveTeamNameRequest:self];
    } else if ( protocol == GSProtocolSignedIn ) {
        _name = [object objectForKey:@"name"];
        if ( [_delegate respondsToSelector:@selector(playerDidReceiveSignedIn:)] )
            [_delegate playerDidReceiveSignedIn:self];
    } else if ( protocol == GSProtocolGameBegan ) {
        if ( [_delegate respondsToSelector:@selector(playerDidReceiveGameBegan:)] )
            [_delegate playerDidReceiveGameBegan:self];
    } else if ( protocol == GSProtocolPlayersUpdate ) {
        if ( [_delegate respondsToSelector:@selector(player:didReceivePlayersUpdate:)] )
            [_delegate player:self didReceivePlayersUpdate:[object objectForKey:@"players"]];
    } else if ( protocol == GSProtocolBuzzedEarly ) {
        if ( [_delegate respondsToSelector:@selector(playerDidBuzzEarly:)])
            [_delegate playerDidBuzzEarly:self];
    } else if ( protocol == GSProtocolWonBuzzIn ) {
        if ( [_delegate respondsToSelector:@selector(playerDidWinBuzz:)] )
            [_delegate playerDidWinBuzz:self];
    } else {
        NSLog(@"Error: received unknown protocol: %@", @(protocol));
    }
}

- (void) connectionDidClose:(DTBonjourDataConnection *)connection {
    NSLog(@"Connection closed");
}

@end


