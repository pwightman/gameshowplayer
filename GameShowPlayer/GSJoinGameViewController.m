//
//  GSJoinGameViewController.m
//  GameShowPlayer
//
//  Created by Parker Wightman on 3/12/13.
//  Copyright (c) 2013 Alora Studios. All rights reserved.
//

#import "GSJoinGameViewController.h"
#import <DTBonjour/DTBonjourDataConnection.h>
#import "GSPlayer.h"
#import <PSAlertView/PSPDFAlertView.h>
#import "GSLobbyViewController.h"

#define GSGameBonjourType @"_ComAloraStudiosGameShow._tcp."

@interface GSJoinGameViewController () <NSNetServiceBrowserDelegate, GSPlayerDelegate>

@property (strong, nonatomic)        NSMutableArray         *foundServices;
@property (weak, nonatomic) IBOutlet UITableView            *tableView;
@property (strong, nonatomic)        NSNetServiceBrowser    *browser;

@property (strong, nonatomic) GSPlayer *player;

@end

@implementation GSJoinGameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _foundServices = [NSMutableArray array];
    
    _browser = [[NSNetServiceBrowser alloc] init];
    
    _browser.delegate = self;
    
    [self beginSearching];
	// Do any additional setup after loading the view.
}

- (void) netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didNotSearch:(NSDictionary *)errorDict {
    NSLog(@"Error searching %@", errorDict);
}

- (void) netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didFindService:(NSNetService *)aNetService moreComing:(BOOL)moreComing {
    [_foundServices addObject:aNetService];
    NSLog(@"Found service: %@", aNetService.name);
    [_tableView reloadData];
}

- (void) netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didRemoveService:(NSNetService *)aNetService moreComing:(BOOL)moreComing {
    [_foundServices removeObject:aNetService];
    [_tableView reloadData];
}

- (void) netServiceBrowserDidStopSearch:(NSNetServiceBrowser *)aNetServiceBrowser {
    NSLog(@"Stopped searching, starting again...");
    [self beginSearching];
}

- (void) beginSearching {
    [_browser searchForServicesOfType:GSGameBonjourType inDomain:@""];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _foundServices.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.textLabel.text = [[_foundServices objectAtIndex:indexPath.row] name];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNetService *service = [_foundServices objectAtIndex:indexPath.row];
    
    DTBonjourDataConnection *dataConnection = [[DTBonjourDataConnection alloc] initWithService:service];
    _player = [GSPlayer playerWithDataConnection:dataConnection];
    _player.delegate = self;
    
    [_player sendSignIn];
}



- (void) playerDidReceiveTeamNameRequest:(GSPlayer *)player {
    PSPDFAlertView *alertView = [[PSPDFAlertView alloc] initWithTitle:@"What's Your Team Name?"];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    __block NSString *name = nil;
    __block __weak PSPDFAlertView *blockAlertView = alertView;
    
    [alertView addButtonWithTitle:@"Send" block:^{
        name = [[blockAlertView textFieldAtIndex:0] text];
        _player.name = name;
        [_player sendTeamName];
    }];
    
    [alertView show];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"lobby"]) {
        GSLobbyViewController *controller = (GSLobbyViewController *)segue.destinationViewController;
        controller.player = _player;
    }
}

- (void) playerDidReceiveSignedIn:(GSPlayer *)player {
    NSLog(@"Signed in!!!");
    
    [self performSegueWithIdentifier:@"lobby" sender:self];
}
@end
