//
//  GSPlayer.h
//  GameShowPlayer
//
//  Created by Parker Wightman on 3/12/13.
//  Copyright (c) 2013 Alora Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DTBonjour/DTBonjourDataConnection.h>

@protocol GSPlayerDelegate;

@interface GSPlayer : NSObject

@property (weak, nonatomic) id<GSPlayerDelegate> delegate;
@property (strong, nonatomic) DTBonjourDataConnection *dataConnection;

@property (strong, nonatomic) NSString *name;

+ (id) playerWithDataConnection:(DTBonjourDataConnection *)dataConnection;

- (void) sendBuzzIn;
- (void) sendSignIn;
- (void) sendTeamName;
    
@end



@protocol GSPlayerDelegate <NSObject>

@optional

- (void) playerDidReceiveSignedIn:(GSPlayer *)player;
- (void) playerDidReceiveGameBegan:(GSPlayer *)player;
- (void) playerDidReceiveBeginBuzzing:(GSPlayer *)player;
- (void) playerDidReceiveOtherBuzzedIn:(GSPlayer *)player;
- (void) playerDidReceiveTeamNameRequest:(GSPlayer *)player;
- (void) playerDidWinBuzz:(GSPlayer *)player;
- (void) playerDidBuzzEarly:(GSPlayer *)player;
- (void) player:(GSPlayer *)player didReceivePlayersUpdate:(NSArray *)players;

@end