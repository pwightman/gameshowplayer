//
//  GSJoinGameViewController.h
//  GameShowPlayer
//
//  Created by Parker Wightman on 3/12/13.
//  Copyright (c) 2013 Alora Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GSJoinGameViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
