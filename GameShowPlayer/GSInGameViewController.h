//
//  GSInGameViewController.h
//  GameShowPlayer
//
//  Created by Parker Wightman on 3/12/13.
//  Copyright (c) 2013 Alora Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSPlayer.h"

@interface GSInGameViewController : UIViewController <GSPlayerDelegate>

@property (strong, nonatomic) GSPlayer *player;
@property (strong, nonatomic) NSArray *players;

@end
