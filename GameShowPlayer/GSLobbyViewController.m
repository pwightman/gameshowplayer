//
//  GSLobbyViewController.m
//  GameShowPlayer
//
//  Created by Parker Wightman on 3/12/13.
//  Copyright (c) 2013 Alora Studios. All rights reserved.
//

#import "GSLobbyViewController.h"
#import <DTBonjourDataConnection.h>
#import "GSPlayer.h"
#import "GSInGameViewController.h"

@interface GSLobbyViewController () <GSPlayerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *hiLabel;
@property (strong, nonatomic) NSArray *players;

@end

@implementation GSLobbyViewController

- (void)viewDidLoad
{
    _hiLabel.text = [NSString stringWithFormat:@"Hi %@", _player.name];
    _player.delegate = self;
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (IBAction)buzzTapped:(id)sender {
    [_player sendBuzzIn];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"game"]) {
       GSInGameViewController *controller = (GSInGameViewController *)segue.destinationViewController;
        controller.player = _player;
        controller.players = _players;
    }

}

- (void) playerDidReceiveGameBegan:(GSPlayer *)player {
    [self performSegueWithIdentifier:@"game" sender:self];
}

- (void) player:(GSPlayer *)player didReceivePlayersUpdate:(NSArray *)players {
//    NSLog(@"Got player update: %@", players);
    _players = players;
}

@end
