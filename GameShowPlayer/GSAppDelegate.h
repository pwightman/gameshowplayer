//
//  GSAppDelegate.h
//  GameShowPlayer
//
//  Created by Parker Wightman on 3/11/13.
//  Copyright (c) 2013 Alora Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
