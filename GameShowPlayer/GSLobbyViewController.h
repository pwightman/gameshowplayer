//
//  GSLobbyViewController.h
//  GameShowPlayer
//
//  Created by Parker Wightman on 3/12/13.
//  Copyright (c) 2013 Alora Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GSPlayer;

@interface GSLobbyViewController : UIViewController

@property (strong, nonatomic) GSPlayer *player;

@end
